/**
  * Created by lennartv on 11/8/16.
  */
import breeze.linalg._

class RichDocument (_id:Int, _title:String, _words:Seq[String]) {

  val id:Int = _id
  val title:String = _title

/*  if (_words.isEmpty) {
    println("WARNING: Created empty document!")
    System.exit(-1)
  }*/

  val words:Seq[String] = _words.sortBy((f:String) => f)
  val unique_words:Seq[String] = _words.distinct.sortBy((f:String) => f)
  val word_to_index:Map[String, Int] = unique_words.zipWithIndex.toMap[String, Int]
  val tf:Seq[Int] = calculate_tf()
  val log_tf:Seq[Double] = calculate_log_tf()

  // Auxiliaries
  val num_words = words.length
  val num_unique_words = unique_words.length

  // TODO: Verify (normalize maybe)
  private def calculate_tf():Seq[Int] = {
    words.groupBy(identity).mapValues(l => l.length)
      .toSeq.map((f:(String, Int)) => (word_to_index(f._1), f._2)).sortBy(_._1).map(_._2)
  }

  // TODO: Verify
  private def calculate_log_tf():Seq[Double] = {
    val sum:Double = tf.sum.toDouble
    val vs:Double = tf.length.toDouble

    tf.map((x:Int) => breeze.numerics.log2(1.0 + x.toDouble/(sum + vs)))
  }

  def calculate_global_tf(_vocabulary: Stream[String]): Seq[Int] = {
    // Calculate the tf vector with the given vocabulary (e.g. the unique words of the full corpus)
    val word_to_global_index:Map[String, Int] = _vocabulary.zipWithIndex.toMap[String, Int]
    val idx_val = words.filter((w: String) => _vocabulary.contains(w))
      .groupBy(identity).mapValues(l => l.length)
      .toSeq.map((f: (String, Int)) => (word_to_global_index(f._1), f._2))
    var global_tf = new Array[Int](_vocabulary.length)
    idx_val.foreach((tup: (Int, Int)) => (global_tf(tup._1) = tup._2))
    global_tf
  }

  def calculate_global_log_tf(_vocabulary: Stream[String]): Seq[Double] = {
    val global_tf: Seq[Int] = calculate_global_tf(_vocabulary)
    val sum: Double = global_tf.sum.toDouble
    val vs: Double = global_tf.length.toDouble
    global_tf.map((x:Int) => breeze.numerics.log2(1.0 + x.toDouble/(sum + vs)))
  }

}
