/*
* Created by lennartv on 11/8/16.
*/
import breeze.numerics._

class NaiveBayes(_category:String, _posterior_threshold: Double, _alpha: Double) extends BinaryClassifier(_category) {

  // If posterior is above this classify as belonging to 'this.category'
  val posterior_threshold:Double = _posterior_threshold

  // Smoothing parameter
  val alpha:Double = _alpha

  // Prior (Index 1 <=> doc does not belong to category, Index 2 <=> doc does belong to category)
  var num_docs_in_category:(Int, Int) = (0, 0)
  var priors:(Double, Double) = (0.0, 0.0)

  var num_unique_words_in_corpus:Int = 0
  var num_words_in_category:(Int, Int) = (0, 0)
  var num_words_in_corpus:Int = 0

  // Normalization for word likelihood
  var denominators:(Double, Double) = (0.0, 0.0)

  override def toString(): String = {

    val s0 = "\nReport for category '" + category + "'\n"
    val s1 = "Number of docs per category: " + num_docs_in_category + "\n"
    val s2 = "Priors: " + priors + "\n"
    val s3 = "Number of unique words in corpus: " + num_unique_words_in_corpus + "\n"
    val s4 = "Number of words in corpus: " + num_words_in_corpus + "\n"
    val s5 = "Number of words per category: " + num_words_in_category + "\n"
    val s6 = "Denominators: " + denominators + "\n"

    s0 + s1 + s2 + s3 + s4 + s5 + s6
  }

  override def train(rich_doc_st: Stream[RichDocument], outcome_st: Stream[Boolean]): (Stream[String], Stream[String]) = {

    println("Training on category " + category)
    // Number of documents
    val num_documents:Int = outcome_st.length

    // Documents that -do- belong to category
    val tmp_count:Int = outcome_st.count((p:Boolean) => p)
    num_docs_in_category = num_docs_in_category.copy(_1 = num_documents - tmp_count, _2 = tmp_count)

    // Split into two category streams
    val doc_category_one:Stream[RichDocument] = rich_doc_st.zip(outcome_st).filter(!_._2).map(_._1)
    val doc_category_two:Stream[RichDocument] = rich_doc_st.zip(outcome_st).filter(_._2).map(_._1)

    // Priors
    priors = priors.copy(_1 = num_docs_in_category._1.toDouble/num_documents.toDouble,
      _2 = num_docs_in_category._2.toDouble/num_documents.toDouble)

    // Determine corpus size (in words)
    num_unique_words_in_corpus = rich_doc_st.flatMap((f: RichDocument) => f.unique_words).distinct.length

    // Determine number of words per category
    num_words_in_category = num_words_in_category.copy(
      _1 = doc_category_one.map((f: RichDocument) => f.num_words).sum,
      _2 = doc_category_two.map((f: RichDocument) => f.num_words).sum
    )

    // Determine total number of words in corpus
    num_words_in_corpus = num_words_in_category._1 + num_words_in_category._2

    // Combine into denominators
    denominators = denominators.copy(
      _1 = num_words_in_category._1 + alpha * num_unique_words_in_corpus,
      _2 = num_words_in_category._2 + alpha * num_unique_words_in_corpus)

    print(this)

    return ((doc_category_one.flatMap(_.words), doc_category_two.flatMap(_.words)))
  }

  // Predict 'true' if document belongs to 'this.category'
  override def predict(st: Stream[RichDocument], in:(Stream[String], Stream[String])): Seq[(Boolean, Double)] = {

    var predictions:Seq[(Boolean, Double)] = Vector.empty

    val PwcSparseNumerator_one = in._1.groupBy(identity).mapValues(l=>l.length+1)
    val PwcSparseNumerator_two = in._2.groupBy(identity).mapValues(l=>l.length+1)

    for (rich_doc:RichDocument <- st) {

      val unique_words = rich_doc.unique_words

      // Word likelihoods over corpus
      val word_likelihoods:(Seq[Double], Seq[Double])= (
        unique_words.map((word: String) => (PwcSparseNumerator_one.getOrElse(word, 0).toDouble + alpha)/(num_words_in_category._1.toDouble + alpha * num_unique_words_in_corpus.toDouble)),
        unique_words.map((word: String) => (PwcSparseNumerator_two.getOrElse(word, 0).toDouble + alpha)/(num_words_in_category._2.toDouble + alpha * num_unique_words_in_corpus.toDouble))
        )

      val log_likelihoods = (
        rich_doc.tf.zip(word_likelihoods._1).map((f: (Int, Double)) => f._1.toDouble * log(f._2)).sum,
        rich_doc.tf.zip(word_likelihoods._2).map((f: (Int, Double)) => f._1.toDouble * log(f._2)).sum
        )

      val likelihoods = (exp(log_likelihoods._1), exp(log_likelihoods._2))

      val unnormalized_posteriors = (
        priors._1 * likelihoods._1,
        priors._2 * likelihoods._2
        )

      // Normalize
      val posterior_sum:Double = unnormalized_posteriors._1 + unnormalized_posteriors._2

      val posteriors = (
        unnormalized_posteriors._1/posterior_sum,
        unnormalized_posteriors._2/posterior_sum
        )

      // Classify
      if (posteriors._2 > posterior_threshold) {
        predictions = predictions :+ (true, posterior_threshold)
        println(rich_doc.id + ":" + category)
      } else {
        print(".")
        predictions = predictions :+ (false, posterior_threshold)
      }

    }

    return predictions
  }

}