/**
  * Created by lennartv on 11/8/16.
  */
import scala.reflect.ClassTag

class OneVsAllClassifier[T <% BinaryClassifier :ClassTag](s:Stream[RichDocument],
                                                          outcome:Stream[Set[String]],
                                                          fac: String => T) {

  val CategoryCap:Int = 15

  val unique_categories:Seq[String] = outcome.reduceLeft(_ union _).toSeq
  val num_unique_categories = unique_categories.length
  var binary_classifiers:Seq[T] = Seq.tabulate(num_unique_categories)((i:Int) => fac(unique_categories(i)))
  train() // Start training upon object creation

  private def train():Unit = {

    /*
    Intentionally left blank (Training is done in the predict method)
     */
  }

  def predict(st:Stream[RichDocument]):Seq[Set[String]] = {
/*
    binary_classifiers.map((binary_classifier:T) => binary_classifier.predict(st, binary_classifier.train(s, to_binary_outcome_stream(outcome, binary_classifier.category)))
      .map(if (_) Set[String](binary_classifier.category) else Set[String]()))
      .reduceLeft(pointwise_union)*/

    binary_classifiers.map((binary_classifier:T) => binary_classifier.predict(st, binary_classifier.train(s, to_binary_outcome_stream(outcome, binary_classifier.category)))
      .map((f:(Boolean, Double)) => if (f._1) Set[(String, Double)]((binary_classifier.category, f._2)) else Set[(String, Double)]()))
      .reduceLeft(pointwise_union)
      .map(prune_set)
  }

  private def pointwise_union(a: Seq[Set[(String, Double)]], b: Seq[Set[(String, Double)]]):Seq[Set[(String, Double)]] = {

    a.zip(b).map((f:(Set[(String, Double)], Set[(String, Double)])) => f._1 union f._2)
  }

  private def prune_set(sset: Set[(String, Double)]):Set[String] = {
    sset.toSeq.sortBy((f:(String, Double)) => -f._2).take(CategoryCap).map(_._1).toSet
  }

  private def to_binary_outcome_stream(stream: Stream[Set[String]], category: String):Stream[Boolean] = {
    stream.map((f:Set[String]) => f contains category)
  }

}
