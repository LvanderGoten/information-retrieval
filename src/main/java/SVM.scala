/*
* Created by lennartv on 11/8/16.
*/
import breeze.linalg.{DenseMatrix, DenseVector}
import breeze.numerics._
import org.junit.experimental.theories.DataPoint

class SVM(_category:String, _lambda: Double, _step: Double) extends BinaryClassifier(_category) {

  // Regularization parameter lambda
  val lambda:Double = _lambda

  // actually just a counter of that is incremented according to the Pegasos algorithm.
  var step:Double = _step
  var theta:DenseVector[Double] = DenseVector(0.0)
  // not saved in class for performance reasons
  //var vocabulary: Stream[String] = Stream.empty[String]

  override def toString(): String = {

    val s0 = "\nReport for category '" + category + "'\n"

    s0
  }



  // performs the update step of the Pegasos algorithm
  def updateStep(cur_theta: DenseVector[Double], doc_rep: (Seq[Double], Double)): DenseVector[Double] = {
    step = step + 1
    val x = DenseVector(doc_rep._1.toArray[Double])
    if (step % 100 == 0) {
      // sign of progress
      //println(step)
    }
    // according to Pegasos algo (see http://www.da.inf.ethz.ch/files/ir2016/IR2016-05.Categorization.pdf)
    val thetaShrink = cur_theta * (1-1.0/step)
    val margin = 1.0 - ( doc_rep._2 * cur_theta.dot(x) )
    if (margin <= 0) thetaShrink
    else thetaShrink + (x * ( 1.0 / (lambda * step)) * doc_rep._2 )
  }

  // calculates the updated theta according to the Pegasos algorithm.
  def get_theta(tuples: Stream[(Seq[Double], Double)]) = {
    // we perform the update step iteratively on every document in our stream (represented here by tuples that is x and y)
    // and use the result of the update step as new theta for the next iteration (defauls/first value is the first document.
    tuples.foldLeft(DenseVector(tuples.head._1.toArray[Double]))(updateStep(_, _))
  }

  override def train(rich_doc_st: Stream[RichDocument], outcome_st: Stream[Boolean]): (Stream[String], Stream[String]) = {

    // Number of documents
    val num_documents:Int = outcome_st.length

    val vocabulary = rich_doc_st.map((f: RichDocument) => f.unique_words).flatten.distinct
    // Documents that -do- belong to category
    //val tmp_count:Int = outcome_st.count((p:Boolean) => p)
    //num_docs_in_category = num_docs_in_category.copy(_1 = num_documents - tmp_count, _2 = tmp_count)
    implicit def bool2int(b:Boolean) = if (b) 1 else 0
    val y = outcome_st.map(_*2.0-1.0)
    val log_tf = rich_doc_st.map((doc:RichDocument) => doc.calculate_global_log_tf(vocabulary))
    theta = get_theta(log_tf.zip(y))
    return(vocabulary, Stream.empty)

  }

  // Predict 'true' if document belongs to 'this.category'
  override def predict(st: Stream[RichDocument], in:(Stream[String], Stream[String])): Seq[(Boolean, Double)] = {

    var predictions:Seq[(Boolean, Double)] = Vector.empty

    val vocabulary = in._1
    theta = theta / breeze.linalg.norm(theta)

    for (rich_doc:RichDocument <- st) {
      val log_tf = rich_doc.calculate_global_log_tf(vocabulary)
      val dot_product = DenseVector(log_tf.toArray).dot(theta)

      // Classify
      // F1 score optimizing parameter found by cross validation.
      if (dot_product > -0.001) {
        //println("Assigned '" + rich_doc.title + "' to category " + category + " (Distance to hyperplane = " + dot_product + ")")
        println(rich_doc.id + ":" + category)
        predictions = predictions :+ (true, dot_product)
      } else {
        // mostly as sign of progress
        print(".")
        predictions = predictions :+ (false, dot_product)
      }

    }

    return predictions
  }

}