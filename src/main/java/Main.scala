/**
  * Created by lennartv on 11/8/16.
  */

import ch.ethz.dal.tinyir.io.ReutersRCVStream
import ch.ethz.dal.tinyir.processing.{StopWords, Tokenizer, XMLDocument}
import com.github.aztek.porterstemmer._
import breeze.numerics._
import java.io._

object Main {
  val Classifier: String = "nb"

  // Hyperparameter for models
  val NaiveBayesPosteriorThreshold: Double = 0.6
  val NaiveBayesAlpha: Double = 0.5

  val SVMlambda: Double = 0.1
  val SVMstep: Double = 0.0

  val LRlambda = 0.1
  val LRalphaMinus = 1.0
  val LRalphaPlus = 1.0
  val LRIterations = 5

  // Subset training set (50000 max)
  // Best 20000
  val NumberOfTrainingSamples: Int = 50000
  val NumberOfTrainingSamplesSVM: Int = 1000

  // Subset validation set (10000 max)
  val NumberOfValidationSamples: Int = 250


  def main(args: Array[String]): Unit = {

    // Setup data streams
    val reuters_train = new ReutersRCVStream("data/train")
    println("Number of files in training set: " + reuters_train.length)

    // TODO: Uncomment for validation
    //val reuters_validation = new ReutersRCVStream("data/validation")
    // TODO: Uncomment for validation
    //println("Number of files in validation set: " + reuters_validation.length)

    // TODO: Uncomment for test
    val reuters_test = new ReutersRCVStream("data/test")
    // TODO: Uncomment for test
    println("Number of files in test set: " + reuters_test.length)

    // Preprocess streams
    val reuters_rich_train = Preprocessor.prune_tf_idf(reuters_train.stream.take(NumberOfTrainingSamples))
    // TODO: Uncomment for validation
    //val reuters_rich_validation = Preprocessor.prune_tf_idf(reuters_validation.stream.take(NumberOfValidationSamples))
    // TODO: Uncomment for test
    val reuters_rich_test =  Preprocessor.prune_tf_idf(reuters_test.stream)

    // Retrieve outcome
    val reuters_train_categories = reuters_train.stream.take(NumberOfTrainingSamples).map(Preprocessor.extract_categories)
    // TODO: Uncomment for validation
    //val reuters_validation_categories = reuters_validation.stream.take(NumberOfValidationSamples).map(Preprocessor.extract_categories)

    if (Classifier == "nb") {
      println("Training One-Vs-All.. Naive Bayes")

      val create_factory_nb = { category: String => new NaiveBayes(category, NaiveBayesPosteriorThreshold, NaiveBayesAlpha) }
      val one_vs_all_nb: OneVsAllClassifier[NaiveBayes] = new OneVsAllClassifier[NaiveBayes](reuters_rich_train,
        reuters_train_categories, create_factory_nb)

      // Evaluate on validation set
      println()
      println("Evaluating NB on validation set..")
      // TODO: Uncomment for validation
      //val validation_predictions_nb: Seq[Set[String]] = one_vs_all_nb.predict(reuters_rich_validation)
      // TODO: Uncomment for test
      val test_predictions_nb: Seq[Set[String]] = one_vs_all_nb.predict(reuters_rich_test)

      // Print summary
      // TODO: Uncomment for validation
      //Postprocessor.print_summary(reuters_validation_categories, validation_predictions_nb)

      // Print statistics
      // TODO: Uncomment for validation
      //Postprocessor.print_stats(reuters_validation_categories, validation_predictions_nb)

      // Save output file
      // TODO: Uncomment for validation
      //Postprocessor.print_predictions("nb", reuters_rich_validation, validation_predictions_nb)
      Postprocessor.print_predictions("nb", reuters_rich_test, test_predictions_nb)
    } else if (Classifier == "lr") {
      println("Training One-Vs-All.. Linear Regression")

      val create_factory_lr = { category: String => new LogisticRegression(category, LRlambda, LRalphaMinus, LRalphaPlus, LRIterations) }
      val one_vs_all_lr: OneVsAllClassifier[LogisticRegression] = new OneVsAllClassifier[LogisticRegression](reuters_rich_train,
        reuters_train_categories, create_factory_lr)

      // Evaluate on validation set
      println()
      println("Evaluating LR on validation set..")
      // TODO: Uncomment for validation
      //val validation_predictions_lr: Seq[Set[String]] = one_vs_all_lr.predict(reuters_rich_validation)
      val test_predictions_lr: Seq[Set[String]] = one_vs_all_lr.predict(reuters_rich_test)

      // Print summary
      // TODO: Uncomment for validation
      //Postprocessor.print_summary(reuters_validation_categories, validation_predictions_lr)

      // Print statistics
      // TODO: Uncomment for validation
      //Postprocessor.print_stats(reuters_validation_categories, validation_predictions_lr)

      // Save output file
      // TODO: Uncomment for validation
      //Postprocessor.print_predictions("lr", reuters_rich_validation, validation_predictions_lr)
      Postprocessor.print_predictions("lr", reuters_rich_test, test_predictions_lr)
    } else if (Classifier == "svm") {
      println("Training One-Vs-All.. SVM")

      val create_factory_svm = { category: String => new SVM(category, SVMlambda, SVMstep) }
      val one_vs_all_svm: OneVsAllClassifier[SVM] = new OneVsAllClassifier[SVM](reuters_rich_train.take(NumberOfTrainingSamplesSVM),
        reuters_train_categories.take(NumberOfTrainingSamplesSVM), create_factory_svm)

      // Evaluate on validation set
      println()
      println("Evaluating SVM on validation set..")
      // TODO: Uncomment for validation
      //val validation_predictions_svm: Seq[Set[String]] = one_vs_all_svm.predict(reuters_rich_validation)
      // TODO: Uncomment for test
      val test_predictions_svm:Seq[Set[String]] = one_vs_all_svm.predict(reuters_rich_test)

      // Print summary
      // TODO: Uncomment for validation
      //Postprocessor.print_summary(reuters_validation_categories, validation_predictions_svm)

      // Print statistics
      // TODO: Uncomment for validation
      //Postprocessor.print_stats(reuters_validation_categories, validation_predictions_svm)

      // Save output file
      // TODO: Uncomment for validation
      //Postprocessor.print_predictions("svm", reuters_rich_validation, validation_predictions_svm)
      // TODO: Uncomment for test
      Postprocessor.print_predictions("svm", reuters_rich_test, test_predictions_svm)
    }

  }

  object Preprocessor {
    val TfIdfPercentile: Int = 50

    def prune_tf_idf(doc_st: Stream[XMLDocument]): Stream[RichDocument] = {

      val n: Int = doc_st.length

      val token_st: Stream[Seq[String]] = doc_st.map(get_tokens)

      // Create TF maps
      println("Create TF maps")
      val tf_st: Stream[Map[String, Double]] = token_st.map((f: Seq[String]) => f.groupBy(identity).mapValues(l => log2(1.0 + l.length.toDouble)))

      // Determine vocabulary
      println("Determine vocabulary")
      val vocab: Stream[String] = token_st.flatten.distinct

      val token_unique_st: Stream[Set[String]] = token_st.map(_.toSet)

      // Count in how many documents a word is (IDF)
      println("Determine IDF")
      val df_seq: Stream[Double] = vocab
        .map((word: String) => log2(n.toDouble) - log2(token_unique_st.filter((tokens: Set[String]) => tokens contains word).length.toDouble))
      val df_map: Map[String, Double] = vocab.zip(df_seq).toMap

      // Construct TF-IDF map
      println("Construct TF-IDF map")
      val tf_idf_st: Stream[Map[String, Double]] = tf_st.map((m: Map[String, Double]) => m.toSeq.map((t: (String, Double)) => (t._1, df_map(t._1) * t._2)).toMap)

      // Construct ranking
      println("Construct ranking")
      val word_ranking_seq: Stream[Double] = vocab.map((word: String) => tf_idf_st.map((m: Map[String, Double]) => m.getOrElse(word, 0.0)).sum)

      // Sorted ranking
      //println("Sort ranking")
      val word_ranking = vocab.zip(word_ranking_seq).sortBy(_._2).map(_._2).toIndexedSeq
      val _min = word_ranking(0)
      val _max = word_ranking.last
      val _median = word_ranking(word_ranking.length / 2)
      val _3Q = word_ranking((3 * word_ranking.length) / 4)
      println("[Min = %f, Max = %f, Median = %f, 3Q = %f]" format(_min, _max, _median, _3Q))

      // Words to be removed
      println("Prune words")
      val _threshold = word_ranking((TfIdfPercentile * word_ranking.length) / 100)
      val bad_words: Stream[String] = vocab.zip(word_ranking_seq).sortBy(_._2).filter(_._2 < _threshold).map(_._1)
      println("Pruned " + bad_words.length + " words")

      return doc_st.zip(token_st).map((f: (XMLDocument, Seq[String])) => new RichDocument(f._1.ID, f._1.title, f._2.filter((word: String) => !bad_words.contains(word))))
    }

    def get_tokens(xml_doc: XMLDocument): Seq[String] = {
      // Tokenize
      var tokens: Seq[String] = Tokenizer.tokenize(xml_doc.title)

      // Remove stop words
      tokens = StopWords.filterOutSW(tokens)

      // Lowercase
      tokens = tokens.map(_.toLowerCase)

      // Porter Stemmer
      tokens = tokens.map(PorterStemmer.stem(_))

      return tokens
    }

    def extract_categories(xml_doc: XMLDocument): Set[String] = xml_doc.codes
  }

  object Postprocessor {


    def print_stats(real_categories: Seq[Set[String]], predicted_categories: Seq[Set[String]]): Unit = {

      val num_documents: Int = real_categories.length

      print_separator()
      println("Average precision: " + precision_per_document(real_categories, predicted_categories).sum / num_documents.toDouble)
      println("Average recall: " + recall_per_document(real_categories, predicted_categories).sum / num_documents.toDouble)
      println("Average F-score: " + f_score_per_document(real_categories, predicted_categories).sum / num_documents.toDouble)
    }

    def print_predictions(model: String, input_st: Seq[RichDocument], predicted_categories: Seq[Set[String]]): Unit = {

      val file = new File("ir-2016-1-project-[23]-" + model + ".txt")
      val bw = new BufferedWriter(new FileWriter(file))

      for (f: (RichDocument, Set[String]) <- input_st zip predicted_categories) {
        val id: Int = f._1.id
        var labels = ""

        for (predicted_category <- f._2) {
          labels = labels + predicted_category + " "
        }
        val line: String = id + " " + labels + "\n"

        bw.write(line)
      }

      bw.close()
    }

    def print_summary(real_categories: Seq[Set[String]], predicted_categories: Seq[Set[String]]): Unit = {

      print_separator()
      for ((real_category, predicted_category) <- real_categories zip predicted_categories) {
        println("Expected: " + real_category + ", Predicted: " + predicted_category)
      }
    }

    def precision_per_document(real_categories: Seq[Set[String]],
                               predicted_categories: Seq[Set[String]]): Seq[Double] = {

      real_categories.zip(predicted_categories)
        .map((f: (Set[String], Set[String])) => if (f._2.isEmpty) 0.0 else (f._1 & f._2).size.toDouble / f._2.size.toDouble)
    }

    def recall_per_document(real_categories: Seq[Set[String]],
                            predicted_categories: Seq[Set[String]]): Seq[Double] = {

      real_categories.zip(predicted_categories)
        .map((f: (Set[String], Set[String])) => (f._1 & f._2).size.toDouble / f._1.size.toDouble)
    }

    def f_score_per_document(real_categories: Seq[Set[String]],
                             predicted_categories: Seq[Set[String]],
                             beta: Double = 1.0): Seq[Double] = {

      precision_per_document(real_categories, predicted_categories)
        .zip(recall_per_document(real_categories, predicted_categories))
        .map((f: (Double, Double)) => if (f._1 == 0.0 && f._2 == 0.0) 0.0 else (beta * beta + 1) * f._1 * f._2 / (beta * beta * f._1 + f._2))
    }

    def print_separator() = {
      println()
      println("_____________________")
      println()
    }
  }

}


