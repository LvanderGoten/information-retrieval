/**
  * Created by lennartv on 11/8/16.
  */
abstract class BinaryClassifier (_category:String) {

  val category:String = _category

  def train(rich_doc_st: Stream[RichDocument], outcome_st: Stream[Boolean]): (Stream[String], Stream[String])
  def predict(rich_doc_st: Stream[RichDocument], in:(Stream[String], Stream[String])): Seq[(Boolean, Double)]

}
