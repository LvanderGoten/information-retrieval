import breeze.linalg.DenseVector
import breeze.numerics.sigmoid

class LogisticRegression(_category: String, _lambda: Double, _alphaMinus: Double, _alphaPlus: Double, _iterations: Int) extends BinaryClassifier(_category) {
  val random = scala.util.Random
  var dim = 0

  // Regularization parameter lambda
  val lambda: Double = _lambda

  // Regularization for imbalanced classes
  val alphaMinus = _alphaMinus
  val alphaPlus = _alphaPlus

  // actually just a counter of that is incremented according to the Pegasos algorithm.
  val iterations = _iterations
  var step = 0.0
  var w: DenseVector[Double] = DenseVector(0.0)
  // not saved in class for performance reasons
  //var vocabulary: Stream[String] = Stream.empty[String]

  // Category streams
  var doc_category_one: Stream[RichDocument] = Stream.empty
  var doc_category_two: Stream[RichDocument] = Stream.empty

  override def toString(): String = {
    val s0 = "\nReport for category '" + category + "'\n"
    s0
  }

//  def logistic(x: DenseVector[Double], y: DenseVector[Double]): Double =
//    1.0 / (1.0 + Math.exp(-1.0 * x.dot(y)))

  def update(w: DenseVector[Double], x: DenseVector[Double], y: Boolean) = {
    // val z = if (y) alphaPlus * (1 - logistic_wx) else alphaMinus * -logistic_wx
    val z = if (y) (1 - sigmoid(x.dot(w))) else -sigmoid(x.dot(w))
    x * z
  }

  def update_step(w: DenseVector[Double], xy: (Seq[Double], Boolean)): DenseVector[Double] = {
    step = step + 1
    val x = DenseVector(xy._1.toArray[Double])
    // val y = xy._2
    val gradient = update(w, x, xy._2)
    w - lambda * gradient * (1 / Math.sqrt(step))
  }

  // TODO: Maybe repeat Gradient Descent iterations
  def get_w(tuples: Stream[(Seq[Double], Boolean)]) = {
    w = DenseVector.fill(dim) {
      2 * random.nextDouble - 1
    }
    for (i <- 1 to iterations) {
      w = tuples.foldLeft(w)(update_step(_, _))
    }
  }

  override def train(rich_doc_st: Stream[RichDocument], outcome_st: Stream[Boolean]): (Stream[String], Stream[String]) = {
    val vocabulary = rich_doc_st.flatMap((f: RichDocument) => f.unique_words).distinct
    // Documents that -do- belong to category
    //val tmp_count:Int = outcome_st.count((p:Boolean) => p)
    //num_docs_in_category = num_docs_in_category.copy(_1 = num_documents - tmp_count, _2 = tmp_count)
    val y = outcome_st
    val log_tf = rich_doc_st.map((doc: RichDocument) => doc.calculate_global_log_tf(vocabulary))
    dim = log_tf.head.length
    get_w(log_tf.zip(y))

    (vocabulary, Stream.empty)
  }

  // Predict 'true' if document belongs to 'this.category'
  override def predict(st: Stream[RichDocument], in: (Stream[String], Stream[String])): Seq[(Boolean, Double)] = {
    var predictions: Seq[(Boolean, Double)] = Vector.empty
    val vocabulary = in._1
    println(".")
    w = w / breeze.linalg.norm(w)
    for (rich_doc: RichDocument <- st) {
      val log_tf = rich_doc.calculate_global_log_tf(vocabulary)
      val dot_product = DenseVector(log_tf.toArray).dot(w)

      // Classify
      // F1 score optimizing parameter found by cross validation.
      if (dot_product > 0.0001) {
        //println("Assigned '" + rich_doc.title + "' to category " + category + " (Distance to hyperplane = " + dot_product + ")")
        println(rich_doc.id + ":" + category)
        predictions = predictions :+ (true, dot_product)
      } else {
        print(".")
        predictions = predictions :+ (false, -dot_product)
      }
    }

    predictions
  }
}